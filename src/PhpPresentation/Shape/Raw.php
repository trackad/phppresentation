<?php
/*
 * This file is part of the Trackad package.
 * 
 * (c) Emilien Roux <e.roux@estoreagency.ru>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PhpOffice\PhpPresentation\Shape;

use PhpOffice\PhpPresentation\AbstractShape;
use PhpOffice\PhpPresentation\ComparableInterface;

/**
 * Description of Raw
 *
 * @author Ivan Pavlov <i.pavlov@estoreagency.ru>
 */
class Raw extends AbstractShape implements ComparableInterface
{
    protected $content;

    /**
     * Create a new \PhpOffice\PhpPresentation\Shape\Raw instance
     */
    public function __construct($content  ='')
    {
        // Initialise variables
        $this->content = $content;

        // Initialize parent
        parent::__construct();
    }

    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }
    
    public function setShapeId($shapeId)
    {
        $this->content = str_replace('{{shapeId}}', $shapeId, $this->content);

        return $this;
    }

}
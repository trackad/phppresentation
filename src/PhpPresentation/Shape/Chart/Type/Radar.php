<?php
/*
 * This file is part of the Trackad package.
 * 
 * (c) Emilien Roux <e.roux@estoreagency.ru>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PhpOffice\PhpPresentation\Shape\Chart\Type;

use PhpOffice\PhpPresentation\ComparableInterface;

/**
 * Description of Raw
 *
 * @author Ivan Pavlov <i.pavlov@estoreagency.ru>
 */
class Radar extends AbstractType implements ComparableInterface
{
    /**
     * Get hash code
     *
     * @return string Hash code
     */
    public function getHashCode()
    {
        $hash = '';
        foreach ($this->getSeries() as $series) {
            $hash .= $series->getHashCode();
        }
        return md5($hash . __CLASS__);
    }
}
